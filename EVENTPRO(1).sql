-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 15-04-2024 a las 05:53:29
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `EVENTPRO`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `numero` int(5) NOT NULL,
  `nombre` tinytext NOT NULL,
  `dir_calle` varchar(40) NOT NULL,
  `dir_num` int(5) NOT NULL,
  `dir_colonia` varchar(30) NOT NULL,
  `num_tel` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `municipio` varchar(5) DEFAULT NULL,
  `rfc` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`numero`, `nombre`, `dir_calle`, `dir_num`, `dir_colonia`, `num_tel`, `correo`, `password`, `municipio`, `rfc`) VALUES
(15, 'TSC Miami', 'Camino Vecinal', 11202, 'Colonia Rancho, El Refugio', '664 123 4567', 'TSC@TSC.com', 'TSC123', 'TIJ', 'TSCM12345678'),
(16, 'Southfi-metals', 'C. 11 Nte. 650', 22444, 'Cd Industrial', '663 126 0077', 'Southfi@Southfi.com', 'Southfi123', 'TIJ', 'SM1234567890'),
(17, 'Toyota Tijuana', 'Av. Vía Rápida Pte. 15155', 22010, 'Zona Urbana Rio', '664 103 6699', 'Toyota@Toyota.com', 'Toyota123', 'TIJ', 'TT1234567890'),
(18, 'Comisión de Desarrollo Industrial de Mexicali', 'Calz. Cetys 2600', 21254, 'Rivera', '686 288 1137', 'CDIM@CDIM.com', 'CDIM123', 'MEX', 'CDIM12345678'),
(19, 'BREG Mexico', 'Blvr. Venustiano Carranza', 21385, 'Zona Sin Asignación de Colonia', '800 321 0607', 'BREG@BREG.com', 'BREG123', 'MEX', 'BREGM1234567'),
(20, 'Schlage de Mexico S.A. de C.V.', 'Carr. Transpeninsular 4580-3', 22890, 'El Naranjo', '646 182 3280', 'Schlage@Schlage.com', 'Schlage123', 'ENS', 'SMA1234567890'),
(21, '3M Aearo Technologies de Baja', 'Transpeninsular 394', 22890, 'Carlos Pacheco', '646 910 8909', '3MAearo@3MAearo.com', '3MAearo123', 'ENS', '3MA1234567890'),
(22, 'Instrumentos Musicales Fender', 'C. Huerta 279', 22785, 'El Naranjo', '646 136 1396', 'Fender@Fender.com', 'Fender123', 'ENS', 'IMF1234567890'),
(23, 'Economic Development Commission', 'Boulevard Nuevo León 251 San Fernando', 21450, 'El Pedregal', '665 654 4859', 'Development@Development.com', 'Development123', 'TEC', 'EDC1234567890'),
(24, 'Tecate Furniture Manufacturing', 'Carrizo 1010', 21478, 'La Nopalera', '665 654 2359', 'FurnitureManufacturing@Furniture.com', 'Furniture123', 'TEC', 'TFM1234567890'),
(29, 'Sass', 'Calle 1', 23212, 'Ninguna', '6642025790', 'Juan1499@gmail.com', 'juan1234', 'TIJ', '1243212345678'),
(30, 'Jajs', 'calle12', 122222, 'Nada', '6642223344', 'Jsjd', 'jejejwjw', 'TIJ', '1122334455667'),
(31, 'Pepe', 'Calle1', 12334, 'Numer1', '6642024790', 'Aaaaa@gmail.com', 'iloviu', 'ENS', '1122334455667'),
(32, 'PEDRO ', 'Calle1', 12345, 'Nunguna', '6642025790', 'Pedro@gmail.com', '1234', 'MEX', '1234567890123'),
(33, 'Ness', 'Esmeralda', 14, 'Pedregal', '6644221871', 'Cane@gmail.com', '1234', 'TIJ', 'Hdjwjzjebzjs'),
(34, 'Juan', 'Nose', 11112, 'Ninguna', '6642025790', '1499@gmail.com', 'juan1499', 'TEC', '1234567890123'),
(35, 'Juan avalos', 'No', 1212, 'Ninguna', '6642025790', '1122@gmail.com', 'juan1212', 'ROS', '1212112233445'),
(36, 'Junasa', 'Calle1', 3122, 'Ninguno', '6642021212', '111@gmail.com', 'juan1122', 'ROS', '1234567890987'),
(37, 'Jjjj', 'Jaja', 1112, 'Bada', '6666666222', 'J@gmail.com', 'juan11', 'TEC', '121234657890'),
(39, 'Hola', 'Jaja', 1112, 'Bada', '6666666222', 'JJ@gmail.com', 'juan11', 'TEC', '121234657890'),
(40, 'Hola1', 'Jaja', 1112, 'Bada', '6666666222', 'JJJ@gmail.com', 'juan11', 'TEC', '121234657890'),
(41, 'Juanchito', 'Nada', 12333, 'Si', '6643335790', 'Juan1213', '$2b$10$kuV/QRVwCYrZVGgijKfn1eKpdD1RINW57BaXgepycD.CPnaZ.vc0y', 'ROS', '1234567890987'),
(43, 'po', 'Utt', 1499, 'Refugio', '6642025790', 'eventpro@gmail.com', '$2b$10$H2m/bw34kTuQUIWElr2kMu6kak8u2DixSfHbIHypCr4mcm3zEOhhW', 'TEC', '1234567890123'),
(44, 'Saaa', 'Calle1', 1234, 'Nose', '6642025790', 'Juan111@gmail.com', '$2b$10$NUlLr.DGbTYB.VvSuHUHYuoa152g7J6NQzw9Xg7UiWZLCPWypdoKS', 'MEX', 'LOCA8001013H3'),
(45, 'Pepe jose', 'Calle1', 1234, 'Nimguna', '6642025790', 'Juan12345@gmail.com', '$2b$10$vRVp/Sic/ihIdPRk0tJua.7bHM3g33RJfqGJJjCWp6RZ6rwTsdNlK', 'ROS', 'LLCC8812227A9'),
(46, 'Isaac', 'Calle1', 1212, 'Ninguna', '6642025799', 'Issac1234@gmail.com', '$2b$10$rtO67COf.98Mxxmj4nZMB.wSU.m0PHzArBg3dkZ/G5mpVLLsOIExG', 'ROS', 'MELM8305281H0'),
(47, 'Alexa', 'Malinche', 22, 'Urbi2', '6642025790', 'Alexa@gmail.com', '$2b$10$0UnNULcE1DM7sIaiavAtMudQE1fVdIa0ajgmPclKImR4mXIeuDUFK', 'ENS', 'DIPA041210QQ6'),
(48, 'Pepeee', 'Calle22', 212, 'Juarez', '6642025790', 'A@gmail.com', '$2b$10$5FdAnzYelHh.Q3vEwsuQPeQCJ7LYtF5pqk.10.UjukQYCJKWMntXW', 'ROS', 'LOCL4271738H0'),
(49, 'Traka', 'Aee', 2312, 'Aaaa', '6644233211', 'B@gmail.com', '$2b$10$RQna0f9zfoG8ShaXUyOmYOoBCyBbRGx0yOs8uCJnunQcyQEIr/Cyy', 'TEC', 'ESDF2123443H0'),
(51, 'Cccc', 'Hsjs', 2622, 'Nada', '6432345678', 'C@gmail.com', '$2b$10$xZDWFwAoE1NmhJLjMAtCEeBrLdQ/2tCvc5KsHaBPDA1VX2ZmTcfZi', 'ROS', 'RHJK7312476H0'),
(54, 'juancho', 'calle1', 6621, 'Coliny1', '6642025790', 'J1@gmail.com', '$2b$10$6pyxGSok6Xihs/j.CK4V4O1DFy6gMDgIFm7s9Z0SPcKPohmHlWzRi', 'TEC', 'MELM8345987H0'),
(55, 'Pepepe', 'Calle', 1221, 'Nada', '6642223344', 'Js@gmail.com', '$2b$10$/exbQ2zeem80ffrsJJqooeDirLM17EPf0Ugqi8dueB.b4cwxsOA36', 'ROS', 'MELM8305281H0'),
(56, 'Js', 'Yes', 2211, 'Jaja', '6642021122', '11@gmail.com', '$2b$10$2Ba.LyKIIWNtB.0bpSAenO.JEbc2KKHermon2l8DWlFuMz76fETlu', 'TEC', 'JELM8212765H1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disponibilidad`
--

CREATE TABLE `disponibilidad` (
  `codigo` varchar(5) NOT NULL,
  `descripcion` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `disponibilidad`
--

INSERT INTO `disponibilidad` (`codigo`, `descripcion`) VALUES
('DIS', 'Disponible'),
('NODIS', 'No Disponible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `codigo` varchar(5) NOT NULL,
  `descripcion` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`codigo`, `descripcion`) VALUES
('TER', 'Event completed'),
('PRO', 'Event in process'),
('INI', 'Event started');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluacion`
--

CREATE TABLE `evaluacion` (
  `numero` int(11) NOT NULL,
  `calificacion` decimal(3,2) DEFAULT NULL,
  `cliente` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE `evento` (
  `numero` int(5) NOT NULL,
  `nombre_evento` varchar(25) DEFAULT NULL,
  `fecha_registro` date DEFAULT current_timestamp(),
  `fecha_evento` date NOT NULL,
  `evento_inicio` time NOT NULL,
  `evento_fin` time NOT NULL,
  `duracion` time DEFAULT NULL,
  `cantidad_personas` int(5) NOT NULL,
  `salon` int(5) DEFAULT NULL,
  `hotel` int(5) DEFAULT NULL,
  `estado` varchar(5) DEFAULT NULL,
  `transporte` int(5) DEFAULT NULL,
  `servicios` int(5) DEFAULT NULL,
  `cliente` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`numero`, `nombre_evento`, `fecha_registro`, `fecha_evento`, `evento_inicio`, `evento_fin`, `duracion`, `cantidad_personas`, `salon`, `hotel`, `estado`, `transporte`, `servicios`, `cliente`) VALUES
(5, 'EVENTPRO', '2024-04-12', '2024-04-20', '15:06:00', '19:06:00', '04:00:00', 24, 3, 3, 'INI', 2, 6, 43),
(8, 'Eventpro', '2024-04-12', '2024-04-14', '09:45:00', '21:45:00', '04:00:00', 15, 4, 3, 'INI', 2, 6, 43),
(9, 'Cena de gala', '2024-04-12', '2024-04-19', '10:51:00', '22:51:00', '04:09:00', 25, 2, 4, 'INI', 3, 8, 43),
(10, 'dia del niño', '2024-04-12', '2024-04-30', '17:00:00', '23:00:00', '06:00:00', 90, 2, 2, 'INI', 3, 8, 43),
(11, 'Fiesta de graduacion', '2024-04-12', '2024-04-25', '20:00:00', '00:00:00', '00:00:04', 150, 2, 2, 'INI', 3, 8, 43);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `numero` int(5) NOT NULL,
  `descripcion` varchar(25) DEFAULT NULL,
  `fecha_factura` date DEFAULT NULL,
  `monto_total` decimal(10,2) DEFAULT NULL,
  `sub_total` decimal(10,2) DEFAULT NULL,
  `IVA` decimal(10,2) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`numero`, `descripcion`, `fecha_factura`, `monto_total`, `sub_total`, `IVA`, `cliente`) VALUES
(1, 'EventPro', '2024-04-12', 55000.00, 47413.79, 7586.21, 43);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitaciones`
--

CREATE TABLE `habitaciones` (
  `numero` int(5) NOT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `cantidad_habitaciones` int(5) DEFAULT NULL,
  `capacidad` int(5) DEFAULT NULL,
  `hotel` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `habitaciones`
--

INSERT INTO `habitaciones` (`numero`, `tipo`, `precio`, `cantidad_habitaciones`, `capacidad`, `hotel`) VALUES
(1, 'Sencilla', 1200.00, 50, 3, 1),
(2, 'Doble', 1500.00, 50, 4, 1),
(3, 'Handicap', 1800.00, 23, 3, 1),
(4, 'Grand Tower', 1200.00, 320, 4, 2),
(5, 'Grand Care', 1500.00, 58, 2, 2),
(6, 'Grand Club', 3000.00, 40, 2, 2),
(7, 'Presidental Suite', 5000.00, 4, 4, 2),
(8, 'Single Plus', 2000.00, 40, 2, 3),
(9, 'Double Plus Room', 1800.00, 40, 4, 3),
(10, 'Prime Suite', 2400.00, 40, 2, 3),
(11, 'Accesible Room', 1800.00, 40, 2, 3),
(12, 'Standard Deluxe', 2358.87, 40, 4, 4),
(13, 'Junior Suite', 2624.65, 40, 6, 4),
(14, 'Coral Deluxe', 3189.45, 40, 4, 4),
(15, 'Glamping Airstream', 2500.00, 20, 2, 5),
(16, 'Estándar Tesela', 2800.00, 20, 2, 5),
(17, 'Estándar Encinal', 2800.00, 20, 2, 5),
(18, 'Villa Estándar', 3200.00, 20, 2, 5),
(19, 'Estudio Encinal', 3200.00, 20, 3, 5),
(20, 'Estudio Tesela', 3200.00, 20, 3, 5),
(21, 'Villa Estudio', 3800.00, 20, 3, 5),
(22, 'Villa Master Suite', 6000.00, 20, 5, 5),
(23, 'Tesela Valle', 3200.00, 20, 3, 5),
(24, 'Junior Suite Encinal', 3500.00, 20, 3, 5),
(25, 'LOFT VISTA MAR', 2000.00, 10, 2, 6),
(26, 'LUXURY LOFT VISTA MAR', 2500.00, 10, 2, 6),
(27, 'SUPERIOR VISTA MONTAÑA', 3000.00, 10, 2, 6),
(28, 'SUPERIOR VISTA MAR', 3200.00, 10, 2, 6),
(29, 'SUPERIOR PLUS VISTA MONTAÑA', 3500.00, 10, 3, 6),
(30, 'DELUXE VISTA MAR', 4000.00, 10, 3, 6),
(31, 'PREMIUM VISTA MAR', 5000.00, 10, 4, 6),
(32, 'DELUXE PLUS VISTA MAR', 6000.00, 10, 3, 6),
(33, 'Ejecutive Room', 4000.00, 20, 2, 7),
(34, 'Double Superior', 2800.00, 20, 4, 7),
(35, 'Double Plus', 2800.00, 20, 4, 7),
(36, 'Basic Double Room', 2000.00, 20, 4, 7),
(37, 'Superior Single Room', 2500.00, 20, 2, 7),
(38, 'Basic Single', 1500.00, 20, 2, 7),
(39, 'Estandar', 1200.00, 50, 2, 8),
(40, 'Doble Inn', 1500.00, 50, 4, 8),
(41, 'Suite', 1800.00, 20, 2, 8),
(42, 'Suite Doble', 2000.00, 20, 4, 8),
(43, 'Casa Brisa Mar', 5000.00, 1, 20, 9),
(44, 'Casa Noches de Verano', 5500.00, 1, 12, 9),
(45, 'Casa Luna llena', 6000.00, 1, 20, 9),
(46, 'Casa Mil Flores Misión Viejo', 6200.00, 1, 20, 9),
(47, 'Complejo Privado', 7000.00, 1, 50, 9),
(48, 'Neocolonial Sencilla', 2500.00, 20, 2, 10),
(49, 'Neocolonial Doble', 2800.00, 20, 4, 10),
(50, 'Junior Neocolonial', 2500.00, 20, 2, 10),
(51, 'Junio Estilo California Sencilla', 3200.00, 20, 2, 10),
(52, 'Junio Estilo California Doble', 3200.00, 20, 4, 10),
(53, 'Penthouse', 3200.00, 20, 5, 10),
(54, 'Sunio Beach House', 5000.00, 20, 6, 10),
(55, 'Master Sunio', 3500.00, 20, 2, 10),
(56, 'Sunio Suite', 3200.00, 20, 2, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel`
--

CREATE TABLE `hotel` (
  `numero` int(5) NOT NULL,
  `nombre` text DEFAULT NULL,
  `dir_calle` text DEFAULT NULL,
  `dir_num` int(5) DEFAULT NULL,
  `dir_colonia` text DEFAULT NULL,
  `contacto` varchar(15) DEFAULT NULL,
  `municipio` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `hotel`
--

INSERT INTO `hotel` (`numero`, `nombre`, `dir_calle`, `dir_num`, `dir_colonia`, `contacto`, `municipio`) VALUES
(1, 'Extended Suites Tijuana Macropalza', 'P.º del Río 6662, Río Tijuana 3a. Etapa', 22226, 'Rio Tijuana 3ra Etapa', '664 969 2260', 'TIJ'),
(2, 'Grand Hotel Tijuana', 'Blvd. Agua Caliente 4558', 22014, 'Tijuana 3a Etapa', '664 681 7000', 'TIJ'),
(3, 'Hotel Fiesta Inn Tijuana Airport', 'Rampa Aeropuerto 16000', 22000, 'Aeropuerto, La Pechuga ', '664 979 1900', 'TIJ'),
(4, 'Hotel Coral y Marina', 'Km.103, Carr. Tijuana-Ensenada 3421', 22870, 'Zona Playitas', '646 175 0000', 'ENS'),
(5, 'Maglén Resort', 'México 3 Km. 90.8', 22766, 'Fraccionamiento Las Lomas', '646 152 1284', 'ENS'),
(6, 'Torre Lucerna Hotel Ensenada', 'Carretera Tijuana Ensenada Km 108 5 #2501', 22860, 'Zona Playitas', '646 222 2400', 'ENS'),
(7, 'Santuario Diegueño', 'Río Yaqui 798', 21420, 'Esteban Cantu', '665 654 4777', 'TEC'),
(8, 'Estancia Inn', 'Blvd, Benito Juárez 1450', 21440, 'Encanto Sur', '665 521 3066', 'TEC'),
(9, 'Casa Playa Baja Resort', 'Los Pinos 1', 22714, 'Primo Tapia', '619 987 3034', 'ROS'),
(10, 'Castilos del Mar', 'Carretera Tijuana-Ensenada Km. 29.5', 22711, 'Playas de Rosarito', '661 616 9062', 'ROS'),
(11, 'Rosarito Luxury Penthouse Bobbys By The Sea', 'Carr. Libre Tijuana-Rosarito 43, Veracruz', 22706, 'Playas de Rosarito', '888 360 5936', 'ROS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_asistencia`
--

CREATE TABLE `lista_asistencia` (
  `numero` int(11) NOT NULL,
  `hora_registro` time DEFAULT current_timestamp(),
  `nombre_empleado` varchar(35) DEFAULT NULL,
  `area` varchar(25) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `lista_asistencia`
--

INSERT INTO `lista_asistencia` (`numero`, `hora_registro`, `nombre_empleado`, `area`, `cliente`) VALUES
(74, '00:00:06', 'T', 'Yehe', 43);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `codigo` varchar(5) NOT NULL,
  `nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`codigo`, `nombre`) VALUES
('ENS', 'Ensenada'),
('MEX', 'Mexicali'),
('ROS', 'Rosarito'),
('TEC', 'Tecate'),
('TIJ', 'Tijuana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salon`
--

CREATE TABLE `salon` (
  `numero` int(5) NOT NULL,
  `nombre` text DEFAULT NULL,
  `dir_calle` text DEFAULT NULL,
  `dir_num` int(5) DEFAULT NULL,
  `dir_colonia` text DEFAULT NULL,
  `costo` decimal(10,2) DEFAULT NULL,
  `disponibilidad` varchar(5) DEFAULT NULL,
  `municipio` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `salon`
--

INSERT INTO `salon` (`numero`, `nombre`, `dir_calle`, `dir_num`, `dir_colonia`, `costo`, `disponibilidad`, `municipio`) VALUES
(1, 'Extended Suites Tijuana Macropalza', 'P.º del Río 6662, Río Tijuana 3a. Etapa', 22226, 'Rio Tijuana 3ra Etapa', 10000.00, 'DIS', 'TIJ'),
(2, 'Grand Hotel Tijuana', 'Blvd. Agua Caliente 4558', 22014, 'Aviacion', 10000.00, 'DIS', 'TIJ'),
(3, 'Hotel Fiesta Inn Tijuana Airport', 'Rampa Aeropuerto 16000', 22000, 'Aeropuerto, La Pechuga ', 17000.00, 'DIS', 'TIJ'),
(4, 'Hotel Coral y Marina', 'Km.103, Carr. Tijuana-Ensenada 3421', 22870, 'Zona Playitas', 15000.00, 'DIS', 'ENS'),
(5, 'Maglén Resort', 'México 3 Km. 90.8', 22766, 'Fraccionamiento Las Lomas', 18950.00, 'DIS', 'ENS'),
(6, 'Torre Lucerna Hotel Ensenada', 'Carretera Tijuana Ensenada Km 108 5 #2501', 22860, 'Zona Playitas', 16740.00, 'DIS', 'ENS'),
(7, 'Santuario Diegueño', 'Río Yaqui 798', 21420, 'Esteban Cantu', 10500.00, 'DIS', 'TEC'),
(8, 'Estancia Inn', 'Blvd, Benito Juárez 1450', 21440, 'Encanto Sur', 12200.00, 'DIS', 'TEC'),
(9, 'Casa Playa Baja Resort', 'Los Pinos 1', 22714, 'Primo Tapia', 15500.00, 'DIS', 'ROS'),
(10, 'Castilos del Mar', 'Carretera Tijuana-Ensenada Km. 29.5', 22711, 'Playas de Rosarito', 17800.00, 'DIS', 'ROS'),
(11, 'Rosarito Luxury Penthouse Bobbys By The Sea', 'Carr. Libre Tijuana-Rosarito 43, Veracruz', 22706, 'Playas de Rosarito', 19900.00, 'DIS', 'ROS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios_paquetes`
--

CREATE TABLE `servicios_paquetes` (
  `numero` int(5) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `servicios_paquetes`
--

INSERT INTO `servicios_paquetes` (`numero`, `nombre`, `descripcion`, `precio`) VALUES
(5, 'Basic Package', 'Organize your event with our basic package, which includes essential services for your occasion. As the quality of the package increases, you will enjoy more benefits.', 20000.00),
(6, 'Regular Package', 'Enjoy a step up with our regular event package. In addition to the basic services, get additional benefits that will enhance your experience and comfort.', 28500.00),
(7, 'Premium Package', 'Immerse yourself in excellence with our premium event package. With enhanced services and exclusive benefits, your event will be an unforgettable experience.', 31500.00),
(8, 'Exclusive Package', 'Experience the best of the best with our exclusive event package. Designed for the most demanding clients, this package offers first-class services and exclusive benefits that guarantee a luxury event.', 35000.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transporte`
--

CREATE TABLE `transporte` (
  `numero` int(11) NOT NULL,
  `capacidad` int(5) NOT NULL,
  `calidad` text DEFAULT NULL,
  `costo` decimal(10,2) NOT NULL,
  `cantidad` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `transporte`
--

INSERT INTO `transporte` (`numero`, `capacidad`, `calidad`, `costo`, `cantidad`) VALUES
(1, 30, 'essential', 2000.00, 30),
(2, 30, 'Regular', 4000.00, 25),
(3, 30, 'Premium', 6000.00, 25);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`numero`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD UNIQUE KEY `nombre` (`nombre`) USING HASH,
  ADD KEY `municipio` (`municipio`);

--
-- Indices de la tabla `disponibilidad`
--
ALTER TABLE `disponibilidad`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `evaluacion`
--
ALTER TABLE `evaluacion`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `salon` (`salon`),
  ADD KEY `hotel` (`hotel`),
  ADD KEY `estado` (`estado`),
  ADD KEY `transporte` (`transporte`),
  ADD KEY `servicios` (`servicios`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `fk_cliente_factura` (`cliente`);

--
-- Indices de la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  ADD PRIMARY KEY (`numero`),
  ADD UNIQUE KEY `tipo` (`tipo`),
  ADD KEY `hotel` (`hotel`);

--
-- Indices de la tabla `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`numero`),
  ADD UNIQUE KEY `nombre` (`nombre`) USING HASH,
  ADD KEY `municipio` (`municipio`);

--
-- Indices de la tabla `lista_asistencia`
--
ALTER TABLE `lista_asistencia`
  ADD PRIMARY KEY (`numero`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `salon`
--
ALTER TABLE `salon`
  ADD PRIMARY KEY (`numero`),
  ADD UNIQUE KEY `nombre` (`nombre`) USING HASH,
  ADD KEY `disponibilidad` (`disponibilidad`),
  ADD KEY `municipio` (`municipio`);

--
-- Indices de la tabla `servicios_paquetes`
--
ALTER TABLE `servicios_paquetes`
  ADD PRIMARY KEY (`numero`);

--
-- Indices de la tabla `transporte`
--
ALTER TABLE `transporte`
  ADD PRIMARY KEY (`numero`),
  ADD UNIQUE KEY `calidad` (`calidad`) USING HASH;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `numero` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `evaluacion`
--
ALTER TABLE `evaluacion`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `evento`
--
ALTER TABLE `evento`
  MODIFY `numero` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `numero` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  MODIFY `numero` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `hotel`
--
ALTER TABLE `hotel`
  MODIFY `numero` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `lista_asistencia`
--
ALTER TABLE `lista_asistencia`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `salon`
--
ALTER TABLE `salon`
  MODIFY `numero` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `servicios_paquetes`
--
ALTER TABLE `servicios_paquetes`
  MODIFY `numero` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `transporte`
--
ALTER TABLE `transporte`
  MODIFY `numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`municipio`) REFERENCES `municipio` (`codigo`);

--
-- Filtros para la tabla `evaluacion`
--
ALTER TABLE `evaluacion`
  ADD CONSTRAINT `evaluacion_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`numero`),
  ADD CONSTRAINT `evaluacion_ibfk_3` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`numero`);

--
-- Filtros para la tabla `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `evento_ibfk_1` FOREIGN KEY (`salon`) REFERENCES `salon` (`numero`),
  ADD CONSTRAINT `evento_ibfk_2` FOREIGN KEY (`hotel`) REFERENCES `hotel` (`numero`),
  ADD CONSTRAINT `evento_ibfk_3` FOREIGN KEY (`estado`) REFERENCES `estado` (`codigo`),
  ADD CONSTRAINT `evento_ibfk_4` FOREIGN KEY (`transporte`) REFERENCES `transporte` (`numero`),
  ADD CONSTRAINT `evento_ibfk_5` FOREIGN KEY (`servicios`) REFERENCES `servicios_paquetes` (`numero`),
  ADD CONSTRAINT `evento_ibfk_6` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`numero`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `fk_cliente_factura` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`numero`);

--
-- Filtros para la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  ADD CONSTRAINT `habitaciones_ibfk_1` FOREIGN KEY (`hotel`) REFERENCES `hotel` (`numero`);

--
-- Filtros para la tabla `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `hotel_ibfk_1` FOREIGN KEY (`municipio`) REFERENCES `municipio` (`codigo`);

--
-- Filtros para la tabla `lista_asistencia`
--
ALTER TABLE `lista_asistencia`
  ADD CONSTRAINT `lista_asistencia_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`numero`);

--
-- Filtros para la tabla `salon`
--
ALTER TABLE `salon`
  ADD CONSTRAINT `salon_ibfk_1` FOREIGN KEY (`disponibilidad`) REFERENCES `disponibilidad` (`codigo`),
  ADD CONSTRAINT `salon_ibfk_2` FOREIGN KEY (`municipio`) REFERENCES `municipio` (`codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
