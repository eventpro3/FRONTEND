import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Image } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import * as Animatable from 'react-native-animatable';
import { Ionicons } from '@expo/vector-icons'; 
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';

import API_URL from '../../apiConfig';

const SalonsScreen = ({ navigation }) => {
  const [salons, setSalons] = useState([]);

  useEffect(() => {
    fetchSalons();
  }, []);

  const subHeadingRef = React.useRef(null);

  useFocusEffect(
    React.useCallback(() => {
      if (subHeadingRef.current) {
        subHeadingRef.current.bounceIn(1000);
      }
    }, [])
  );

  const fetchSalons = async () => {
    try {
      const response = await fetch(`${API_URL}/salon`);
      const data = await response.json();
      setSalons(data);
    } catch (error) {
      console.error('Error fetching salons:', error);
    }
  };

  const renderSalon = (item, index) => {
    let imageSource;
    switch (index) {
      case 0:
        imageSource = require('../../assets/salones/1.jpg');
        break;
      case 1:
        imageSource = require('../../assets/salones/2.jpg');
        break;
      case 2:
        imageSource = require('../../assets/salones/3.jpg');
        break;
        case 3:
        imageSource = require('../../assets/salones/4.jpg');
        break;
        case 4:
        imageSource = require('../../assets/salones/5.jpg');
        break;
        case 5:
        imageSource = require('../../assets/salones/6.jpg');
        break;
        case 6:
        imageSource = require('../../assets/salones/7.jpg');
        break;
        case 7:
        imageSource = require('../../assets/salones/8.jpg');
        break;
        case 8:
        imageSource = require('../../assets/salones/9.jpg');
        break;
        case 9:
        imageSource = require('../../assets/salones/10.jpg');
        break;
        case 10:
        imageSource = require('../../assets/salones/11.jpg');
        break;
        case 11:
        imageSource = require('../../assets/salones/1.jpg');
        break;
      default:
        imageSource = null;
    }

    return (
      <Animatable.View animation="fadeInUp" style={styles.salonContainer} key={index}>
        <Image source={imageSource} style={styles.salonImage} />
        <Text style={styles.salonName}>{item.nombre}</Text>
        <Text style={styles.salonAddress}>{item.direccion}</Text>
        <TouchableOpacity
          style={styles.descriptionButton}
          onPress={() => toggleDescription(index)}
        >
          <Text style={styles.descriptionButtonText}>Description</Text>
        </TouchableOpacity>

        {item.showDescription && (
          <Animatable.View animation="fadeIn" style={styles.descriptionContainer}>
            <Text style={styles.salonDescription}>Capacity: {item.capacidad}</Text>
            <Text style={styles.salonDescription}>Cost: {item.costo}</Text>
            <Text style={styles.salonDescription}>Available: {item.disponible ? "Disponible" : "Disponible"}</Text>
            <Text style={styles.salonDescription}>City: {item.municipio}</Text>
          </Animatable.View>
        )}
      </Animatable.View>
    );
  };

  const toggleDescription = (index) => {
    setSalons(prevState =>
      prevState.map((item, i) => {
        if (i === index) {
          return { ...item, showDescription: !item.showDescription };
        }
        return item;
      })
    );
  };

  return (
    <DrawerSceneWrapper>
      <ScrollView contentContainerStyle={styles.container}>
        <TouchableOpacity onPress={() => navigation.openDrawer()} style={styles.menuButton}>
          <Ionicons name="menu" size={26} color="#009688" />
        </TouchableOpacity>
        <View style={styles.bottomSpace}></View>
        <Text style={styles.heading}>List of Salons</Text>
        <Animatable.Text ref={subHeadingRef} animation="bounceIn" style={styles.subHeading}>Check out your options!</Animatable.Text>
        {salons.map((item, index) => (
          <Animatable.View key={index} animation="fadeInUp" delay={index * 100}>
            {renderSalon(item, index)}
          </Animatable.View>
        ))}
        <View style={styles.bottomSpace}></View>
      </ScrollView>
    </DrawerSceneWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  subHeading: {
    fontSize: 24,
    marginBottom: 20,
    textAlign: 'center',
    color: '#009688',
    fontWeight: 'bold',
  },
  salonContainer: {
    marginBottom: 30,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: '#ffffff',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  salonImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  salonName: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 5,
    textAlign: 'center',
    backgroundColor: 'white', 
  },
  salonAddress: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: 'center',
    backgroundColor: 'white', 
  },
  descriptionButton: {
    backgroundColor: 'rgba(0, 150, 136, 0.7)',
    paddingVertical: 8,
    paddingHorizontal: 20,
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: 10,
  },
  descriptionButtonText: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 16,
  },
  descriptionContainer: {
    paddingHorizontal: 15,
    marginTop: 10,
  },
  salonDescription: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: 'center',
    backgroundColor: 'white',
  },
  bottomSpace: {
    paddingBottom: 80,
    backgroundColor: 'white', 
  },
  menuButton: {
    position: 'absolute',
    top: 35,
    left: 20,
    zIndex: 1, 
  },
});

export default SalonsScreen;
