import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import Animated, { FadeIn, FadeInDown } from 'react-native-reanimated';
import { useNavigation } from '@react-navigation/native';
import { Calendar } from 'react-native-calendars';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Picker } from '@react-native-picker/picker';
import API_URL from '../../apiConfig'; // Importa la URL de la API desde apiConfig.js

export default function RegisterForm() {
  const navigation = useNavigation();
  const [registrationDate, setRegistrationDate] = useState('');
  const [eventDate, setEventDate] = useState('');
  const [eventStartTime, setEventStartTime] = useState('');
  const [eventEndTime, setEventEndTime] = useState('');
  const [duration, setDuration] = useState('');
  const [numberOfPeople, setNumberOfPeople] = useState('');
  const [eventName, setEventName] = useState('');
  const [transportOptions, setTransportOptions] = useState([]);
  const [selectedTransport, setSelectedTransport] = useState('');
  const [showRegistrationCalendar, setShowRegistrationCalendar] = useState(false);
  const [showEventCalendar, setShowEventCalendar] = useState(false);
  const [isTimePickerVisible, setTimePickerVisible] = useState(false);
  const [showStartTimePicker, setShowStartTimePicker] = useState(true);
  const [showTransportOptions, setShowTransportOptions] = useState(false);

  useEffect(() => {
    fetchTransportOptions();
  }, []);

  const fetchTransportOptions = async () => {
    try {
      const response = await fetch(`${API_URL}/transporte`); 
      const data = await response.json();
      setTransportOptions(data);
    } catch (error) {
      console.error('Error al obtener opciones de transporte:', error);
    }
  };

  const handleSubmit = async () => {
    try {
      const response = await fetch(`${API_URL}/evento`, { 
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          nombre_evento: eventName,
          fecha_registro: registrationDate,
          fecha_evento: eventDate,
          evento_inicio: eventStartTime,
          evento_fin: eventEndTime,
          duracion: duration,
          cantidad_personas: numberOfPeople,
          transporte: selectedTransport, 
        }),
      });
      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.error('Error al enviar formulario:', error);
    }
  };
  const handleDayPressRegistration = (day) => {
    const selectedDate = day.dateString.split('T')[0];
    setRegistrationDate(selectedDate);
    setShowRegistrationCalendar(false);
  };
  
  const handleDayPressEvent = (day) => {
    const selectedDate = day.dateString.split('T')[0];
    setEventDate(selectedDate);
    setShowEventCalendar(false);
  };

  const showTimePicker = () => {
    setTimePickerVisible(true);
  };

  const hideTimePicker = () => {
    setTimePickerVisible(false);
  };

  const handleTimeConfirm = (time) => {
    const formattedTime = `${time.getHours()}:${time.getMinutes()}`;
    if (showStartTimePicker) {
      setEventStartTime(formattedTime);
    } else {
      setEventEndTime(formattedTime);
    }
    hideTimePicker();
  };

  const handleTransportPress = () => {
    setShowTransportOptions(!showTransportOptions);
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <StatusBar style="dark" />
      <Animated.View entering={FadeIn.duration(1000).springify()} style={[styles.titleContainer, { backgroundColor: 'white', marginTop: 0 }]}>
        <Text style={styles.titleText}>Event Registration</Text>
      </Animated.View>
    
      <Animated.View entering={FadeIn.delay(200).duration(1000).springify()} style={styles.formContainer}>
        <Animated.View entering={FadeInDown.delay(400).duration(1000).springify()} style={styles.inputContainer}>
          <TouchableOpacity onPress={() => setShowRegistrationCalendar(!showRegistrationCalendar)} style={styles.dateContainer}>
            <Text style={styles.dateText}>Registration Date: {registrationDate}</Text>
          </TouchableOpacity>
          {showRegistrationCalendar && (
            <Calendar
              onDayPress={handleDayPressRegistration}
              style={styles.calendar}
            />
          )}
        </Animated.View>

        <Animated.View entering={FadeInDown.delay(600).duration(1000).springify()} style={styles.inputContainer}>
          <TouchableOpacity onPress={() => setShowEventCalendar(!showEventCalendar)} style={styles.dateContainer}>
            <Text style={styles.dateText}>Event Date: {eventDate}</Text>
          </TouchableOpacity>
          {showEventCalendar && (
            <Calendar
              onDayPress={handleDayPressEvent}
              style={styles.calendar}
            />
          )}
        </Animated.View>

        <Animated.View entering={FadeInDown.delay(800).duration(1000).springify()} style={styles.inputContainer}>
          <TouchableOpacity onPress={() => { setShowStartTimePicker(true); showTimePicker(); }}>
            <Text style={styles.input}>{eventStartTime ? `Event Start Time: ${eventStartTime}` : 'Select Event Start Time'}</Text>
          </TouchableOpacity>
        </Animated.View>

        <Animated.View entering={FadeInDown.delay(1000).duration(1000).springify()} style={styles.inputContainer}>
          <TouchableOpacity onPress={() => { setShowStartTimePicker(false); showTimePicker(); }} >
            <Text style={styles.input}>{eventEndTime ? `Event End Time: ${eventEndTime}` : 'Select Event End Time'}</Text>
          </TouchableOpacity>
        </Animated.View>

        <DateTimePickerModal
          isVisible={isTimePickerVisible}
          mode="time"
          onConfirm={handleTimeConfirm}
          onCancel={hideTimePicker}
        />

        <Animated.View entering={FadeInDown.delay(1200).duration(1000).springify()} style={styles.inputContainer}>
          <TextInput
            placeholder="Event Duration (hh:mm)"
            placeholderTextColor={'gray'}
            style={styles.input}
            value={duration}
            onChangeText={text => setDuration(text)}
          />
        </Animated.View>

        <Animated.View entering={FadeInDown.delay(1400).duration(1000).springify()} style={styles.inputContainer}>
          <TextInput
            placeholder="Number of People"
            placeholderTextColor={'gray'}
            style={styles.input}
            keyboardType="numeric"
            value={numberOfPeople}
            onChangeText={text => setNumberOfPeople(text)}
          />
        </Animated.View>

        <Animated.View entering={FadeInDown.delay(1600).duration(1000).springify()} style={styles.inputContainer}>
          <TextInput
            placeholder="Event Name"
            placeholderTextColor={'gray'}
            style={styles.input}
            value={eventName}
            onChangeText={text => setEventName(text)}
          />
        </Animated.View>

        <Animated.View entering={FadeInDown.delay(1800).duration(1000).springify()} >
          <TouchableOpacity onPress={handleTransportPress} style={styles.inputContainer}>
            <Text style={{ color: 'black' }}>{selectedTransport ? selectedTransport : 'Seleccionar Transporte'}</Text>
          </TouchableOpacity>
        </Animated.View>


        {showTransportOptions && (
          <Picker
            selectedValue={selectedTransport}
            onValueChange={(itemValue, itemIndex) => setSelectedTransport(itemValue)}
          >
            {transportOptions.map(option => (
              <Picker.Item 
                key={option.numero} 
                label={option.calidad} 
                value={option.numero} // Cambiado a enviar el número del transporte
                style={option.numero === selectedTransport ? styles.selectedOption : styles.option} 
              />
            ))}
          </Picker>
        )}

        <Animated.View entering={FadeInDown.delay(2000).duration(1000).springify()} style={styles.buttonContainer}>
          <TouchableOpacity onPress={handleSubmit} style={styles.button}>
            <Text style={styles.buttonText}>Create</Text>
          </TouchableOpacity>
        </Animated.View>
      </Animated.View>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 20,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5,
    backgroundColor: 'white',
    padding: 11,
  },
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    padding: 20,
  },
  titleContainer: {
    alignItems: 'center',
    marginBottom: 40,
  },
  titleText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 22,
    marginTop: 15,
  },
  formContainer: {
    marginBottom: 25,
  },
 
  buttonContainer: {
    marginBottom: 20,
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#009688',
    borderRadius: 5,
    paddingVertical: 10,
    width: '50%',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  selectedOption: {
    backgroundColor: '#e0e0e0',
    padding: 11,
    borderRadius: 5,
  },
  option: {
    padding: 11,
    borderRadius: 5,
  },
 
  dateText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  calendar: {
    marginBottom: 10,
  },
});
