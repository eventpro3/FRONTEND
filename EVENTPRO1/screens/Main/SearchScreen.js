import React, { useState } from 'react';
import { View, StyleSheet, useWindowDimensions } from 'react-native';
import SearchInput from '../../components/Search/SearchInput';
import Tabs from '../../components/shared/Tabs';
import SearchMasonry from '../../components/Search/SearchMasonry';
import { SEARCH_ALL, SEARCH_HOTELS, SEARCH_PLACES } from '../../data';
import { colors, spacing } from '../../constants/theme';
import * as Animatable from 'react-native-animatable';

const SearchScreen = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const window = useWindowDimensions();

  const handleSearch = (query) => {
    setSearchQuery(query);
  };

  const normalizedQuery = searchQuery.toLowerCase();

  const filteredSearchAll = SEARCH_ALL.filter(
    (item) =>
      (item.title && item.title.toLowerCase().includes(normalizedQuery)) ||
      (item.description && item.description.toLowerCase().includes(normalizedQuery)) ||
      (item.location && item.location.toLowerCase().includes(normalizedQuery))
  );

  const filteredSearchPlaces = SEARCH_PLACES.filter(
    (item) =>
      (item.title && item.title.toLowerCase().includes(normalizedQuery)) ||
      (item.description && item.description.toLowerCase().includes(normalizedQuery)) ||
      (item.location && item.location.toLowerCase().includes(normalizedQuery))
  );

  const filteredSearchHotels = SEARCH_HOTELS.filter(
    (item) =>
      (item.title && item.title.toLowerCase().includes(normalizedQuery)) ||
      (item.location && item.location.toLowerCase().includes(normalizedQuery))
  );

  const tabs = [
    {
      title: 'All',
      content: () => <SearchMasonry key="all" list={filteredSearchAll} />,
    },
    {
      title: 'Municipalities',
      content: () => <SearchMasonry key="places" list={filteredSearchPlaces} />,
    },
    {
      title: 'Hotels',
      content: () => <SearchMasonry key="hotels" list={filteredSearchHotels} />,
    },
  ];

  return (
    <View style={[styles.container, { height: window.height - 100 }]}>
      {/* Espacio donde estaba el título */}
      <View style={styles.titleSpace} />
      
      <SearchInput onSearch={handleSearch} inputStyle={{ color: 'black' }} />
      <Tabs items={tabs} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    paddingHorizontal: spacing.medium,
  },
  titleSpace: {
    height: 30, 
  },
});

export default SearchScreen;
