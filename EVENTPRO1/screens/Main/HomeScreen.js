import React from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Text, SafeAreaView, Platform } from 'react-native';
import { colors as themeColors } from '../../constants/theme';

import ScreenHeader from '../../components/shared/ScreenHeader';
import TopPlacesCarousel from '../../components/Home/TopPlacesCarousel';
import SectionHeader from '../../components/shared/SectionHeader';
import TripsList from '../../components/Home/TripsList';
import { PLACES, TOP_PLACES } from '../../data';
import DrawerSceneWrapper from '../../components/menu/DrawerSceneWrapper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const HomeScreen = ({ navigation }) => {
  const openDrawer = () => {
    navigation.openDrawer();
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <DrawerSceneWrapper>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <TouchableOpacity onPress={openDrawer} style={styles.menuButton}>
              <Icon name="menu" size={24} color="#009688" />
            </TouchableOpacity>
            <Text style={styles.headerTitle}>EVENTPRO</Text>
          </View>
          
          <ScreenHeader mainTitle="Find Your" secondTitle="Best option" />
          
          <ScrollView contentContainerStyle={styles.scrollViewContent} showsVerticalScrollIndicator={false}>
            <TopPlacesCarousel list={TOP_PLACES} />
            <SectionHeader
              title="Popular municipalities"
              buttonTitle=""
              onPress={() => {}}
            />
            <TripsList list={PLACES} />
          </ScrollView>
        </View>
      </DrawerSceneWrapper>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 0 : Platform.OS === 'ios' ? Platform.Version < 11 ? 20 : 0 : 0,
    backgroundColor: themeColors.light,
  },
  container: {
    flex: 1,
    backgroundColor: 'white', 
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 12,
    backgroundColor: 'white',
    borderRadius: 0,
    marginVertical: 12,
    elevation: 2,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowRadius: 10,
  },
  menuButton: {
    marginRight: 12,
  },
  headerTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  scrollViewContent: {
    flexGrow: 1,
    paddingBottom: 65,
  },
});

export default HomeScreen;
