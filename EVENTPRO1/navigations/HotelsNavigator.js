// HotelsNavigator.js
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HotelsCarousel from '../components/TripsDetails/TripDetailsCard/TripDetailsCard/HotelsCarousel';
import HotelDetailsScreen from '../components/Hotel/HotelDetailsScreen';

const HotelsStack = createNativeStackNavigator();

function HotelsNavigator() {
  return (
     <HotelsStack.Navigator initialRouteName="HotelsCarousel">
       <HotelsStack.Screen name="HotelsCarousel" component={HotelsCarousel} options={{ headerShown: false }} />
       <HotelsStack.Screen name="HotelDetailsScreen" component={HotelDetailsScreen} options={{ headerShown: false }} />
     </HotelsStack.Navigator>
  );
 }
 
 export default HotelsNavigator;